package week1.day1;

public class Mobile {
	
	public static void main(String[] args) {
		Mobile call = new Mobile();
		call.sendMsg();
		call.makeCall();
		call.saveContact();
	
		
	}
	
	
	public void sendMsg() {
		System.out.println("Message sent to ");
	}
	
	public void makeCall() {
		System.out.println("Making call ");
	}
	
	public void saveContact() {
		System.out.println("Saving the contact ");
		
	}

}
